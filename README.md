# Project Title

Tasks Toolkit

## Getting Started

clone toolkit ssh:
git@gitlab.rebrainme.com:medved.spb/rebrain-devops-task1.git

### Prerequisites

1. Watch video on rebrain
2. Read instructions for tasks
3. Learn and complete tasks

### Installing

start web preview

1. Install nginx on your PC
2. Replace your nginx on the one in the project
3. Restart nginx / systemd systems: systemctl restart nginx 

## Built With

* [Rebrain](https://rebrainme.com/) - Study platform
* [Nginx](https://nginx.org/) - Nginx Corp
* [Gitlab](https://gitlab.rebrainme.com/) - Web Tool for Git 

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

The version depends on your progress in your studies.

## Authors

* **OOO Rebrain** - (https://lk.rebrainme.com/select-course)

See also the list of [contributors] (https://files.rebrainme.com/privacy.pdf)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Hunger for knowledge
